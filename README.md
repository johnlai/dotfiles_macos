# dotfiles_macos

![Presentation](https://gitlab.com/johnlai/dotfiles_macos/raw/7591900c2256fd56d89ff2232a9c49f0df119b42/images/ss-1-min.png "Presentation")

![Touchbar Presentation](https://gitlab.com/johnlai/dotfiles_macos/raw/master/images/mockup-touchbar-crop.png "Touchbar Presentation")

# Images

![Busy Screenshot](https://gitlab.com/johnlai/dotfiles_macos/raw/7591900c2256fd56d89ff2232a9c49f0df119b42/images/ss-2-min.png "Busy Screenshot")

![Screenshot](https://gitlab.com/johnlai/dotfiles_macos/raw/7591900c2256fd56d89ff2232a9c49f0df119b42/images/ss-3-min.png "Screenshot")

![Touchbar Presentation](https://gitlab.com/johnlai/dotfiles_macos/raw/master/images/mockup-touchbar.png "Touchbar ")

![Touchbar Real Photo](https://gitlab.com/johnlai/dotfiles_macos/raw/20d268922839656c054736f051611225f6bcc080/images/real-tb-photo-min.png "Touchbar Real Photo")



## Softwares

* Iterm2
* Macfeh
* Yabai
* SKHD
* ZSH with ohmyzsh
* ZSH Theme Powerlevel9k
* Neofetch
* Ranger
* user/EmpressNoodle
* Brew
* Übersicht
* Top Bar zzzeyez/Pecan
